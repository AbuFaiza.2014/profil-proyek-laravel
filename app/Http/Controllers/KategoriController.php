<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Kategori;
use RealRashid\SweetAlert\Facades\Alert;


use Illuminate\Support\Facades\Redirect;

class KategoriController extends Controller
{
    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request ){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        DB::table('kategori')->insert([
            'nama' => $request['nama'],
            'deskripsi' => $request['deskripsi'],
        ]);

        Alert::success('Berhasil', 'Tambah Data Kategori Berhasil');

        return redirect('/kategori/create');
        
    }

    public function index(){
        $kategori = Kategori::all();
        return view('kategori.index', compact('kategori'));
    }

    public function show($id){
        $kategori = Kategori::find($id);
        return view('kategori.show', compact('kategori'));
    }

    public function edit($id){
        $kategori = DB::table('kategori')->where('id', $id)->first();
        return view('kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        $query = DB::table('kategori')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'deskripsi' => $request['deskripsi']
              ]);

        Alert::success('Update', 'Data Kategori Berhasil diupdate');


        return Redirect('/kategori');

    }

    public function destroy($id){
        DB::table('kategori')->where('id', $id)->delete();

        Alert::success('Delete', 'Data Kategori Berhasil dihapus');

        return redirect('/kategori');
    }
}
