@extends('layout.master')

@section('judul')
Halaman Tambah Berita   
@endsection

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endpush

@section('content')

<form action="/berita" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Berita</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>content</label>
      <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>kategori</label> <br>
        <select name="kategori_id" class="js-example-basic-single" style="width: 100%" id="">
            <option value="">---Pilih Kategori---</option>
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
      </div>
      @error('kategori_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>thumbnail</label>
        <input type="file" name="thumbnail" class="form-control" >
      </div>
      @error('thumbnail')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection