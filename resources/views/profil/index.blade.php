@extends('layout.master')

@section('judul')
Halaman Update Profil  
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/cv5cbaz81xttj5sozk4zq057tfy587xo5k5at1wwqd2ss0k8/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script> 
@endpush

@section('content')
<form action="/profil/{{$profil->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama User</label>
        <input type="text" value="{{$profil->user->name}}" class="form-control" disabled>
      </div>
      <div class="form-group">
        <label>Email User</label>
        <input type="text" value="{{$profil->user->email}}" class="form-control" disabled>
      </div>

    <div class="form-group">
      <label>Umur Profil</label>
      <input type="number" name="umur" value="{{$profil->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" class="form-control">{{$profil->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control">{{$profil->alamat}}</textarea>
      </div>
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection